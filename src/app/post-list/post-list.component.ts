import { Component, OnInit } from '@angular/core';
import {Post} from '../Post';

@Component({
  selector: 'app-post-list',
  templateUrl: './post-list.component.html',
  styleUrls: ['./post-list.component.css']
})
export class PostListComponent implements OnInit {

  posts: Post[] = [
      new Post('Mon premier post', 'Duplexque isdem diebus acciderat malum, quod et Theophilum insontem atrox interceperat casus.', 2),
      new Post('Mon deuxième post', 'Duplexque isdem diebus acciderat malum, quod et Theophilum insontem atrox interceperat casus.', -1),
      new Post('Encore un post', 'Duplexque isdem diebus acciderat malum, quod et Theophilum insontem atrox interceperat casus.', 0)
  ];
  constructor() { }

  ngOnInit() {
  }

}
